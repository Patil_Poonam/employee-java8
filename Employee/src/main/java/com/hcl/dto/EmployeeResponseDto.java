package com.hcl.dto;

public class EmployeeResponseDto {
	private int employeeId;

	private String employeeName;

	private int age;

	private double salary;

	private String designation;

	private Long phoneNo;
	

	public EmployeeResponseDto() {
		super();
	}

	public EmployeeResponseDto(int employeeId, String employeeName, int age, double salary, String designation,
			Long phoneNo) {
		super();
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.age = age;
		this.salary = salary;
		this.designation = designation;
		this.phoneNo = phoneNo;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Long getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(Long phoneNo) {
		this.phoneNo = phoneNo;
	}


}
