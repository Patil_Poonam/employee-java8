package com.hcl.dto;

public class EmployeeDto {
	private String employeeName;

	private String designation;

	public EmployeeDto() {
		super();
	}

	public EmployeeDto(String employeeName, String designation) {
		super();
		this.employeeName = employeeName;
		this.designation = designation;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}
}
