package com.hcl.controller;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.dto.EmployeeDto;
import com.hcl.dto.EmployeeRequestDto;
import com.hcl.dto.EmployeeResponseDto;
import com.hcl.exception.EmployeeException;
import com.hcl.model.Employee;
import com.hcl.service.EmployeeService;




@RestController
@RequestMapping("/employee")
public class EmployeeController {
	
	@Autowired
	EmployeeService employeeService;
	
	@PostMapping("/employees")
	public ResponseEntity<String> saveEmployee(@RequestBody EmployeeRequestDto employeeRequestDto) throws EmployeeException{
		return new ResponseEntity<String>(employeeService.saveEmployee(employeeRequestDto),HttpStatus.CREATED);
		
	}
	@GetMapping("/employees/lowerSalary")
	public ResponseEntity<List<EmployeeDto>> havingLessSalary(@RequestParam double salary) throws EmployeeException{
		return new ResponseEntity<List<EmployeeDto>>(employeeService.havingLessSalary(salary),HttpStatus.OK);
	}
	
	@GetMapping("/employees/higherSalary")
	public ResponseEntity<List<EmployeeResponseDto>> havingGreaterSalary(@RequestParam double salary) throws EmployeeException{
		return new ResponseEntity<List<EmployeeResponseDto>>(employeeService.havingGreaterSalary(salary),HttpStatus.OK);
	}
	
	
	
	@GetMapping("/employees/hike")
	public ResponseEntity<Map<String,Double>> hikeForThoseHavingLessSalary(@RequestParam double salary,@RequestParam double providedHike)
			throws EmployeeException{
		return new ResponseEntity<Map<String,Double>>(employeeService.hikeThoseHavinglessSalary(salary, providedHike),HttpStatus.OK);
	}
}
